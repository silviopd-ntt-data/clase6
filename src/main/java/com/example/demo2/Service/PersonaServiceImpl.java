package com.example.demo2.Service;

import com.example.demo2.Entity.Persona;
import com.example.demo2.Repository.PersonaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonaServiceImpl implements IPersonaService{

    @Autowired
    private PersonaRepository personaRepository;

    @Override
    public List<Persona> listarPersonas() {
        return personaRepository.findAll();
    }

    @Override
    public Persona buscarPersona(Long id) {
        return personaRepository.getById(id);
    }

    @Override
    public Persona registrarPersona(Persona persona) {
        return personaRepository.save(persona);
    }

    @Override
    public Persona modificarPersona(Long id, Persona persona) {
        persona.setId(id);
        return personaRepository.save(persona);
    }

    @Override
    public void eliminarPersona(Long id) {
        personaRepository.deleteById(id);
    }


}
