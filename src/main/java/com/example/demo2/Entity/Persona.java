package com.example.demo2.Entity;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

//@Getter
//@Setter
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "persona")
public class Persona {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nombres,apellidos;
    private Integer edad;

    @Column(name = "fechanac")
    private Date fechaNacimiento;

    @Transient
    private Double sueldo;

}
